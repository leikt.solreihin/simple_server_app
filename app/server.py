#! /usr/bin/env python3
# coding: utf-8

import socket, sys, threading

def accept_client():
  while 1:
    # accept
    client_socket, client_adress = server_socket.accept()

    # information
    name = client_socket.recv(1024).decode("Utf8")
    connexions[name] = client_socket
    ThreadClient(client_socket, name).start()
    print("{n} entered the chat.".format(n=name))

class ThreadClient(threading.Thread):
  def __init__(self, conn, name):
    threading.Thread.__init__(self)
    self.connexion = conn
    self.name = name

  def run(self):
    while 1:
      msg = self.connexion.recv(1024).decode("Utf8")
      if msg.upper() == "END":
        break
      full_msg = "{n} > {m}".format(n=self.name, m=msg)
      print(full_msg)
      for name in connexions:
        if name != self.name:
          connexions[name].send(full_msg.encode("Utf8"))

    full_msg = "{n} left the chat".format(n=self.name)
    print(full_msg)
    for name in connexions:
      if name != self.name:
        connexions[name].send(full_msg.encode("Utf8"))
        
    self.connexion.close()
    del connexions[self.name]


if __name__ == "__main__":
  connexions = {}

  # socket
  server_socket = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)

  # bind
  host = '2a01:cb15:8250:c000:587a:1901:e3d6:b36'
  port = 24880
  server_socket.bind((host, port))

  # listen
  server_socket.listen(2)
  print("Chat server started on port {p}".format(p=str(port)))

  threading.Thread(target= accept_client).start()
