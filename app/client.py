#! /usr/bin/env python3
# coding: utf-8

import socket, sys, threading

def send():
  while 1:
    msg = input("\nMe > ")
    client_socket.send(msg.encode("Utf8"))
    if msg.upper() == "END":
      break
  # Close connexion
  print("Connexion interrupted by client.")
  client_socket.close()

def recv():
  while 1:
    msg = client_socket.recv(1024).decode("Utf8")
    print("\n" + msg)
    if msg.upper() == "END":
      break
  # Close connexion
  print("Connexion interrupted by server.")
  client_socket.close()

if __name__ == "__main__":
  # Socket
  client_socket = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)

  # Connect
  host = '2a01:cb15:8250:c000:587a:1901:e3d6:b36'
  port = 24880
  try:
    client_socket.connect((host, port))
  except socket.error as e:
    print("Connexion failed.")
    print(e)
    input("Press enter...")
    sys.exit()

  # Send information on the client
  name = input("Name ?\n")
  client_socket.send(name.encode("Utf8"))
  
  thread_send = threading.Thread(target = send)
  thread_send.start()

  thread_recv = threading.Thread(target = recv)
  thread_recv.start()

